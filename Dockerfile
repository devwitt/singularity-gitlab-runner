# Create an image based on the Ubuntu GitLab Runner...
FROM gitlab/gitlab-runner:ubuntu

# Set GO Variables
ENV GO_VERSION=1.13.5
ENV GO_OS=linux
ENV GO_ARCH=amd64
ENV GO_PATH=${HOME}/go
ENV PATH=/usr/local/go/bin:${PATH}:${GO_PATH}/bin

# Set Singularity Version
ENV SINGULARITY_VERSION=3.8.1

# Set variables
ENV USER=gitlab-runner

# Install the Singularity preq's
RUN apt-get update && apt-get install -y \
    build-essential \
    uuid-dev \
    libgpgme-dev \
    squashfs-tools \
    libseccomp-dev \
    wget \
    pkg-config \
    git \
    cryptsetup-bin \
    bash \
    sudo

# Setup GO
RUN wget https://dl.google.com/go/go${GO_VERSION}.${GO_OS}-${GO_ARCH}.tar.gz && \
    tar -C /usr/local -xzvf go${GO_VERSION}.${GO_OS}-${GO_ARCH}.tar.gz && \
    rm go${GO_VERSION}.${GO_OS}-${GO_ARCH}.tar.gz

# Build and setup Singularity
RUN wget https://github.com/sylabs/singularity/releases/download/v${SINGULARITY_VERSION}/singularity-ce-${SINGULARITY_VERSION}.tar.gz && \
    tar -xzf singularity-ce-${SINGULARITY_VERSION}.tar.gz && \
    cd singularity-ce-${SINGULARITY_VERSION} && \
    ./mconfig && \
    make -C ./builddir && \
    make -C ./builddir install && \
    ./mconfig --prefix=/opt/singularity && \
    cd / && \
    rm -r /singularity-ce-${SINGULARITY_VERSION}/ && \
    rm /singularity-ce-${SINGULARITY_VERSION}.tar.gz && \
    apt-get purge -y --auto-remove build-essential

# Grant the gitlab-runner no password sudo, but only to the Singularity executable.
RUN echo "$USER ALL=(ALL) NOPASSWD: /usr/local/bin/singularity" > /etc/sudoers.d/$USER \
        && chmod 0440 /etc/sudoers.d/$USER

LABEL "Maintainer"="Chad DeWitt"
LABEL "Description"="Docker image used to allow a Gitlab Runner to perform Singularity operations."
LABEL version="v0.1"
