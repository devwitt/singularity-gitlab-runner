# singularity-runner-in-docker

This project builds a Docker image, based on the Ubuntu GitLab runner image, which includes Singularity 3.8.0.

First, run the image in registration mode to register with GitLab:

    docker run -it --name gitlab-runner \
     -v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
     -v /var/run/docker.sock:/var/run/docker.sock \
     registry.gitlab.com/devwitt/singularity-gitlab-runner register

Then delete the container and restart:

    docker run -d --name gitlab-runner --privileged --restart always \
         -v /Users/Shared/gitlab-runner/config:/etc/gitlab-runner \
         -v /var/run/docker.sock:/var/run/docker.sock \
         registry.gitlab.com/devwitt/singularity-gitlab-runner

Note that this image must be run in **privileged** mode within Docker to allow the building of Singularity images.
